//
//  AssistiveVisionUITests.swift
//  AssistiveVisionUITests
//
//  Created by Bruno Filipe Costa Horta on 12/12/2017.
//

import XCTest
import CoreData
class AssistiveVisionUITests: XCTestCase {
    let app = XCUIApplication()
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        
        XCUIDevice.shared.orientation = .portrait
       

        app.launchArguments += ["UI-Testing"]

        app.launch()

        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        app.launchArguments.removeAll()
        super.tearDown()
    }
    
    func testTurnOnAndTurnOffVibration() {
        app.buttons["Vibration ON"].tap()
        app.buttons["Vibration OFF"].tap()
    

    }
    func testTurnOnAndTurnOffSound() {
        app.buttons["Sound ON"].tap()
        app.buttons["Sound OFF"].tap()
    
    }

    func testFavoriteAndUnFavorite(){
        
        let tablesQuery = XCUIApplication().tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["aeroplane - NOT FAVORITE"]/*[[".cells.staticTexts[\"aeroplane - NOT FAVORITE\"]",".staticTexts[\"aeroplane - NOT FAVORITE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["bicycle - NOT FAVORITE"]/*[[".cells.staticTexts[\"bicycle - NOT FAVORITE\"]",".staticTexts[\"bicycle - NOT FAVORITE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["bird - NOT FAVORITE"]/*[[".cells.staticTexts[\"bird - NOT FAVORITE\"]",".staticTexts[\"bird - NOT FAVORITE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["boat - NOT FAVORITE"]/*[[".cells.staticTexts[\"boat - NOT FAVORITE\"]",".staticTexts[\"boat - NOT FAVORITE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["aeroplane -  FAVORITE"]/*[[".cells.staticTexts[\"aeroplane -  FAVORITE\"]",".staticTexts[\"aeroplane -  FAVORITE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["bicycle -  FAVORITE"]/*[[".cells.staticTexts[\"bicycle -  FAVORITE\"]",".staticTexts[\"bicycle -  FAVORITE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["bird -  FAVORITE"]/*[[".cells.staticTexts[\"bird -  FAVORITE\"]",".staticTexts[\"bird -  FAVORITE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["boat -  FAVORITE"]/*[[".cells.staticTexts[\"boat -  FAVORITE\"]",".staticTexts[\"boat -  FAVORITE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        
    }
    
    

}
