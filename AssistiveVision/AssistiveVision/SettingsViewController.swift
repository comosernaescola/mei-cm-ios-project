
import UIKit
import AudioToolbox
import AVFoundation
import CoreData
class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var tags: [NSManagedObject] = []
    let synth = AVSpeechSynthesizer()
    
    @IBOutlet weak var vibrationButton: UIButton!
    
    @IBOutlet weak var soundButton: UIButton!
    
    @IBOutlet weak var favoriteTagTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillTagsFromCoreData()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tag = tags[indexPath.row]
        toggleFavorite(tag:tag)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        synth.stopSpeaking(at: .immediate)
    }
  
    override func viewDidAppear(_ animated: Bool) {
        speak(frase: "Settings Mode, Activated.")
        if !UserDefaults.standard.bool(forKey: "intro") {
            fillTagsFromFile()
            UserDefaults.standard.set(true, forKey: "VIBRATE_ON")
            UserDefaults.standard.set(true, forKey: "SOUND_ON")
            UserDefaults.standard.set(true, forKey: "intro")
            
        }
      
        speak(frase: "Rotate phone to landscape for objects detection.")
        vibrationButton.setTitle("Vibration \(UserDefaults.standard.bool(forKey:  "VIBRATE_ON") ? "ON" : "OFF")", for: .normal)
        soundButton.setTitle("Sound \(UserDefaults.standard.bool(forKey: "SOUND_ON") ? "ON" : "OFF")", for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tagCell")
        let tag  = tags[indexPath.row]
        cell?.textLabel?.text =  "\(tag.value(forKey: "tagDescription") as! String) - \(tag.value(forKey: "favorite") as! Bool ? "" : "NOT") FAVORITE"
        return cell!
    }
    
    func fillTagsFromFile() {
        if let path = Bundle.main.path(forResource: "voc", ofType: "txt") {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                data.components(separatedBy: .newlines).filter({ (a) -> Bool in
                    a.count > 0
                }).forEach({ (t) in
                    save(description: t,favorite:false)
                })
            } catch {
                print(error)
            }
        }
    }
    
    @IBAction func onSoundControlClick(_ sender: UIButton) {
        let state = UserDefaults.standard.bool(forKey: "SOUND_ON")
        UserDefaults.standard.set(!state, forKey: "SOUND_ON")
        
        sender.setTitle("Sound \(!state ? "ON" : "OFF")", for: .normal)
    }
    
    func speak(frase: String){
        if UserDefaults.standard.bool(forKey: "SOUND_ON"){
            let utterance =  AVSpeechUtterance(string: "\(frase)")
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            synth.speak(utterance)
        }
    }
    
    @IBAction func onVibrationControlClick(_ sender: UIButton) {
        let state = UserDefaults.standard.bool(forKey: "VIBRATE_ON")
        UserDefaults.standard.set(!state, forKey: "VIBRATE_ON")
        sender.setTitle("Vibration \(!state ? "ON" : "OFF")", for: .normal)
    }
    
    func rotated(){
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            performSegue(withIdentifier: "detectObjectSegue", sender: nil)
            break
        default: break
        }
    }
    func save(description: String, favorite: Bool) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Tags",
                                       in: managedContext)!
        
        let tag = NSManagedObject(entity: entity,
                                  insertInto: managedContext)
        
        tag.setValue(description, forKeyPath: "tagDescription")
        tag.setValue(favorite, forKeyPath: "favorite")
        do {
            try managedContext.save()
            fillTagsFromCoreData()
            favoriteTagTable.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    func toggleFavorite(tag: NSManagedObject) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext =
            appDelegate.persistentContainer.viewContext
        tag.setValue(!(tag.value(forKey: "favorite") as! Bool), forKeyPath: "favorite")
        do {
            try managedContext.save()
            fillTagsFromCoreData()
            favoriteTagTable.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    func fillTagsFromCoreData(){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Tags")
        
        
        do {
            tags = try managedContext.fetch(fetchRequest)
            favoriteTagTable.reloadData()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
}
