

import UIKit
import AudioToolbox
import AVFoundation
import CoreData

class CameraViewController: UIViewController {
    
    var Tags: [NSManagedObject] = []
    
    var results = UITextView()
    var boxesView = UIView()
    var  dragPoint = CGPoint()
    var dragging = false
        override func viewDidLoad() {
        super.viewDidLoad()
        self.view.accessibilityElementsHidden = true
        self.accessibilityElementsHidden = true
            self.accessibilityTraits = UIAccessibilityTraitAllowsDirectInteraction | UIAccessibilityTraitUpdatesFrequently
            

            self.view.accessibilityTraits = UIAccessibilityTraitAllowsDirectInteraction | UIAccessibilityTraitUpdatesFrequently
            

        NotificationCenter.default.addObserver(self, selector: #selector(CameraViewController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        setupCam()
        async {
            self.loadYoloModel()
            frameProcessing = { frame in
                frame.accessibilityElementsHidden = true
                self.detectYoloObjects(frameImage: frame)
            }
        }
        setResultDisplay()
     
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(userPanned))
        panRecognizer.minimumNumberOfTouches = 1
            panRecognizer.accessibilityTraits = UIAccessibilityTraitAllowsDirectInteraction | UIAccessibilityTraitUpdatesFrequently
            

        view.addGestureRecognizer(panRecognizer)
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        synth.stopSpeaking(at: .immediate)
    }
    override func viewDidAppear(_ animated: Bool) {
        speak(frase: "Scaning Mode, Activated,Lets go to detect coolest objects.")
         speak(frase: "Drag your finger on the screen to hear description objects.")
        
    }
   
    func rotated(){
        switch UIDevice.current.orientation {
        case .portrait:
            yolo.clean()
            jpac.clean()
            self.dismiss(animated: true, completion: nil)
            break
        default: break
            
        }
    }
    @objc private func userPanned(recognizer: UIPanGestureRecognizer) {
        if recognizer.state == .changed {
            dragPoint = recognizer.location(in: self.view)
            dragging = true
        }else{
            dragging = false
            lastObject = ""
            dragPoint = CGPoint()
        }
        
        
    }
    func setResultDisplay(){
        self.results.frame = CGRect(x: 20, y: 20, width: self.view.frame.width/2, height: 60)
        self.results.textColor = UIColor.green
        self.results.backgroundColor = UIColor.clear
        self.results.accessibilityElementsHidden = true
        self.view.addSubview(self.results)
        self.view.bringSubview(toFront: self.results)
        self.boxesView.accessibilityElementsHidden = true
        self.boxesView.frame = self.view.frame
        self.boxesView.accessibilityTraits = UIAccessibilityTraitNone
        self.boxesView.backgroundColor = UIColor.clear
        self.view.addSubview(boxesView)
        
        self.view.bringSubview(toFront: self.boxesView)
        
        self.view.layoutSubviews()
    }
    
}

let jpac = Jetpac()

let yolo = YOLO()
var yoloThreshold = 0.25
var detectedObjects : YoloOutput = []
var detectedResults = [[String]]()

// TEXT TO SPEECH
let synth = AVSpeechSynthesizer()
var lastObject = ""


extension CameraViewController {
    func loadYoloModel() {
        yolo.load()
        jpac.load()
        
    }
   
    func detectYoloObjects(frameImage:CIImage){
        
        yolo.threshold = yoloThreshold
        detectedObjects = yolo.run(image: frameImage)
        detectedObjects
            .forEach {
                print($0.label, $0.prob, $0.box);
                detectedResults.append(["\($0.label)", "\($0.prob)", "\($0.box)"])
        }
        DispatchQueue.main.sync() {
            self.results.text = ""
            self.cleanView(someView: self.boxesView)
            if detectedObjects.isEmpty == false{
                let numOfObjects = detectedObjects.count
                for i in 0..<numOfObjects{
                    self.results.text.append( "\(i) : \(detectedObjects[i].label) \n")
                    let box = detectedObjects[i].box
                    let plotView = PlotView(frame: box)
                    plotView.backgroundColor = UIColor.clear
                    plotView.draw(CGRect(x: box.origin.x, y: box.origin.y, width: box.size.width, height: box.size.height))
                    if  self.dragging && self.dragPoint.x >= box.origin.x   && self.dragPoint.x <= box.size.width + box.origin.x {
                        if lastObject != detectedObjects[i].label {
                            lastObject = detectedObjects[i].label
                            speak( frase: detectedObjects[i].label)
                        }
                    }
                    plotView.accessibilityElementsHidden = true
                    self.boxesView.addSubview(plotView)
                    vibrate(tag: detectedObjects[i].label)
                }
            }
        }
    }
    func speak(frase: String){
        if UserDefaults.standard.bool(forKey: "SOUND_ON"){
            let utterance =  AVSpeechUtterance(string: "\(frase)")
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            synth.speak(utterance)
        }
    }
    // VIBRATE
    func vibrate(tag: String){
        if UserDefaults.standard.bool(forKey: "VIBRATE_ON") && checkIfTagIsFavorite(tag: tag){
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
    }
    func checkIfTagIsFavorite(tag:String) -> Bool{
        
            
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return false
            }
            
            let managedContext =
                appDelegate.persistentContainer.viewContext
            
            
            let fetchRequest =
                NSFetchRequest<NSManagedObject>(entityName: "Tags")
        let predicate  = NSPredicate(format: "tagDescription == %@", tag)
            fetchRequest.predicate = predicate
            do {
              let  tagBd = try managedContext.fetch(fetchRequest)
                return tagBd.first?.value(forKey: "favorite") as! Bool
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
        
        return false
        
    }
    func cleanView(someView: UIView){
        for childView in someView.subviews{
            childView.removeFromSuperview()
        }
    }
    
}

extension String {
    func isEqualToString(find: String) -> Bool {
        return String(format: self) == find
    }
}

public class PlotView: UIView
{
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func draw(_ frame: CGRect)
    {
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(4.0)
        context?.setStrokeColor(UIColor.blue.cgColor)
        print("frame: \(frame)")
        context?.addRect(frame)
        context?.strokePath()
    }
    
    
  
}
